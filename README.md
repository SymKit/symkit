SymKit
========================

Welcome to the [SymKit Project](http://symkit.com) - a fully-functional Content Management System utilizing the Symfony Framework application.

For details on how to download and get started with **SymKit CMS**, see the
[Installation](#) chapter of the SymKit Documentation.

What's inside?
--------------

**SymKit CMS** is configured with the following defaults:

  * An Admin area;

  * User management;

  * Blogging system;

  * EU Cookie Compliant site visitor notification;

  * Static Pages.


**SymKit CMS** is released under the MIT.

Enjoy!
